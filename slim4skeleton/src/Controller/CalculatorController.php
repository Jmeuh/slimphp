<?php
namespace App\Controller;

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;
use Respect\Validation\Validator;

final class CalculatorController extends BaseController {

    public function calculator(Request $request, Response $response) {
        $params = $request->getQueryParams();
        $operator = $params['operator'];
        if($operator === "+") {
            $result = $params['value1'] + $params['value2'];
        } else if($operator === "-") {
            $result = $params['value1'] - $params['value2'];
        } else if($operator === "*") {
            $result = $params['value1'] * $params['value2'];
        } else if($operator === "/") {
            $result = $params['value1'] / $params['value2'];
        }
        return $this->render($request, $response, 'calculator.twig', ['result' => $result, "operator" => $operator, "value" => $value]);
    }
}