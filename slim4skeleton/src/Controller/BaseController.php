<?php
namespace App\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

abstract class BaseController {
	protected $view;
	protected $logger;
	protected $flash;
	protected $em;  // Entities Manager
	private $container;

	public function __construct(ContainerInterface $container) {
		$this->view = $container->get('view');
		$this->logger = $container->get('logger');
		$this->flash = $container->get('flash');
		$this->em = $container->get('em');
		$this->container = $container;
	}

	protected function render(Request $request, Response $response, string $template, array $params = []) {

		return $this->view->render($response, $template, $params);
	}
}
