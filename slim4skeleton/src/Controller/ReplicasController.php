<?php
namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ReplicasController extends BaseController {

    private $list = ['Kaamelott', 'The Big Bang Theory', 'Citations'];
    private $value = 1;

    public function replicas(Request $request, Response $response) {
        return $this->render($request, $response, 'replicaslist.twig', ['list' => $this->list]);
    }

    public function kaamelottReplicas(Request $request, Response $response) {
        $replicas = [['Le gras c’est la vie !', 'Karadoc'], ['Si vous prenez aujourd’hui, que vous comptez sept jours, on retombe le même jour mais une semaine plus tard… Enfin à une vache près, c’est pas une science exacte.', 'Karadoc'], ["Ah, le printemps ! La nature se réveille, les oiseaux reviennent, on crame des mecs.", 'Arthur'],["Quand vous vous l'vez pour bouffer, vous me réveillez, vous bougez, vous allumez les bougies, vous toussez, bref ! Vous ignorez mon existence ! ...Eh ben là, pas un bruit, pas un mouvement, tout dans le furtif. Vous avez glissé du pageot comme un pet sur une plaque de verglas !",'Dame Séli'],["Et ben, on est pas sortis du sable !!", 'Guenièvre']];

        return $this->render($request, $response, 'kaamelott.twig', ['replicas' => $replicas, 'list' => $this->list, 'value' => $this->value]);
    }

    public function bigBangTheoryReplicas(Request $request, Response $response) {
        $replicas = [['Je ne suis pas fou. Ma mère m’a fait passer des tests.','Sheldon Cooper'], ['Sur le plan biologique, Howard a raison de chercher une partenaire optimale pour perpétuer sa descendance. En revanche, on peut de se demander si c’est dans l’intérêt de l’Humanité. ', 'Sheldon Cooper']];

        return $this->render($request, $response, 'big-bang-theory.twig', ['replicas' => $replicas, 'list' => $this->list, 'value' => $this->value]); 
    }

    public function Quotes(Request $request, Response $response) {
        $replicas = [["Il n'y a pas homme plus courageux au monde que celui qui réussit à s'arrêter après la première cacahuète!", "Michel Galabrus"],["De tous ceux qui n'ont rien à dire, les plus agréables sont ceux qui se taisent","Coluche"],["Si vous jugez un poisson sur ses capacités à grimper à un arbre, il passera sa vie à croire qu’il est stupide.", 'Albert Einstein']];

        return $this->render($request, $response, 'citations.twig', ['replicas' => $replicas, 'list' => $this->list, 'value' => $this->value]);
    }
}