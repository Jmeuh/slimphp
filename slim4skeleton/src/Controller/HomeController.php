<?php
namespace App\Controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

final class HomeController extends BaseController {

	public function home(Request $request, Response $response) {
		return $this->render($request, $response, 'home.twig', ['date' => $date]);
	}
}
