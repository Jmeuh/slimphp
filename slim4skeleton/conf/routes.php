<?php
declare(strict_types=1);

use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
	$app->get('/', 'App\Controller\HomeController:home')->setName('root');
	$app->get('/presentation', 'App\Controller\PresentationController:presentation')->setName('presentation');
	$app->get('/repliques', 'App\Controller\ReplicasController:replicas')->setName('replicas');
	$app->get('/kaamelott', 'App\Controller\ReplicasController:kaamelottReplicas')->setName('kaamelott');
	$app->get('/big-bang-theory', 'App\Controller\ReplicasController:bigBangTheoryReplicas')->setName('big-bang-theory');
	$app->get('/citations', 'App\Controller\ReplicasController:Quotes')->setName('citations');
	$app->get('/calculator', 'App\Controller\CalculatorController:calculator')->setName('calculator');

	// $app->get('/post/{id}', 'App\Controller\HomeController:viewPost')->setName('post');

	// $app->group('/member', function (Group $group) {
	// 	$group->map(['GET', 'POST'],'/login', 'App\Controller\AuthController:login')->setName('login');
	// 	$group->get('/logout', 'App\Controller\AuthController:logout')->setName('logout');

	// });
};
