<?php
namespace App\Controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class PresentationController extends Controller {

    public function presentation(RequestInterface $request, ResponseInterface $response) {
        $list = [
            ['UN ANIMAL', 'un aigle'],
            ['UN PLAT', 'un magret de canard avec des pommes de terre sautées'],
            ['UNE CHANSON', "Sweetchild O'Mine", "des Guns N'Roses"],
            ['UN OBJET', "une moto"],
            ['UN FILM', "Ray"]
        ];
        $list2 = [
            ['UN SPORT', "le roller"],
            ['UN SITE WEB', "youtube"],
            ['UNE COULEUR', "du bleu nuit"],
            ['UNE CITATION', " 'Si tes résultat ne sont pas à la hauteur de tes espérances, dis-toi que le grand chêne aussi, un jour, à été un gland.'", 'Lao Tseu'],
            ['UNE REPLIQUE', "'Votre prénom c'est François, c'est juste ? Eh bien lui c'est pareil, c'est Juste.'", 'Le dîner de cons']
        ];
        return $this->render($response, 'pages/presentation.twig', ['list' => $list, 'list2' => $list2]);
    }
}