<?php
namespace App\Controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
// use Respect\Validation\Validator;


class HomeController extends Controller {

    public function home(RequestInterface $request, ResponseInterface $response) {
        $this->render($response, 'pages/home.twig');
    }
}