<?php
namespace App\Controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ReplicasController extends Controller {
    
    public function replicas(RequestInterface $request, ResponseInterface $response) {
        $replicas = ['Kaamelott', 'The Big Bang Theory', 'Citations'];

        return $this->render($response, 'pages/replicaslist.twig', ['replicas' => $replicas]);
    }

    public function kaamelottReplicas(RequestInterface $request, ResponseInterface $response) {
        $replicas = [['Le gras c’est la vie !', 'Karadoc'], ['Si vous prenez aujourd’hui, que vous comptez sept jours, on retombe le même jour mais une semaine plus tard… Enfin à une vache près, c’est pas une science exacte.', 'Karadoc'], ["Ah, le printemps ! La nature se réveille, les oiseaux reviennent, on crame des mecs.", 'Arthur'],["Quand vous vous l'vez pour bouffer, vous me réveillez, vous bougez, vous allumez les bougies, vous toussez, bref ! Vous ignorez mon existence ! ...Eh ben là, pas un bruit, pas un mouvement, tout dans le furtif. Vous avez glissé du pageot comme un pet sur une plaque de verglas !",'Dame Séli'],["Et ben, on est pas sortis du sable !!", 'Guenièvre']];

        return $this->render($response, 'pages/kaamelott.twig', ['replicas' => $replicas]);
    }

    public function bigBangTheoryReplicas(RequestInterface $request, ResponseInterface $response) {
        $replicas = [['Je ne suis pas fou. Ma mère m’a fait passer des tests.','Sheldon Cooper'], ['Sur le plan biologique, Howard a raison de chercher une partenaire optimale pour perpétuer sa descendance. En revanche, on peut de se demander si c’est dans l’intérêt de l’Humanité. ', 'Sheldon Cooper']];

        return $this->render($response, 'pages/big-bang-theory.twig', ['replicas' => $replicas]); 
    }

    public function Quotes(RequestInterface $request, ResponseInterface $response) {
        $replicas = [["Il n'y a pas homme plus courageux au monde que celui qui réussit à s'arrêter après la première cacahuète!", "Michel Galabrus"],["De tous ceux qui n'ont rien à dire, les plus agréables sont ceux qui se taisent","Coluche"],["Si vous jugez un poisson sur ses capacités à grimper à un arbre, il passera sa vie à croire qu’il est stupide.", 'Albert Einstein']];

        return $this->render($response, 'pages/citations.twig', ['replicas' => $replicas]);
    }
}