<?php
use App\Controllers\HomeController;
use App\Controllers\ReplicasController;
use App\Controllers\PresentationController;

require '../vendor/autoload.php';

session_start();

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

require '../app/container.php';

$container = $app->getContainer();

// Middleware
// $app->add(new \App\Middlewares\FlashMiddleware($container->view->getEnvironment()));
// $app->add(new \App\Middlewares\OldMiddleware($container->view->getEnvironment()));
// $app->add(new \App\Middlewares\TwigCsrfMiddleware($container->view->getEnvironment(), $container->csrf));
// $app->add($container->csrf);

$app->get('/', HomeController::class . ':home')->setName('root');
$app->get('/presentation', PresentationController::class . ':presentation')->setName('presentation');
$app->get('/repliques', ReplicasController::class . ':replicas')->setName('replicas');
$app->get('/kaamelott', ReplicasController::class . ':kaamelottReplicas')->setName('kaamelott');
$app->get('/big-bang-theory', ReplicasController::class . ':bigBangTheoryReplicas')->setName('big-bang-theory');
$app->get('/citations', ReplicasController::class . ':Quotes')->setName('citations');


$app->run();